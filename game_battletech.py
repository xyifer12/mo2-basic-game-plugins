# -*- encoding: utf-8 -*-

import os

import mobase
from PyQt6.QtCore import QDir, QFileInfo

from ..basic_game import BasicGame


class BattletechModDataChecker(mobase.ModDataChecker):
    def __init__(self):
        super().__init__()
        self.validDirNames = [
        "Mods",
    ]

    def dataLooksValid(
        self, filetree: mobase.IFileTree
    ) -> mobase.ModDataChecker.CheckReturn:
        for entry in filetree:
            if not entry.isDir():
                continue
            if entry.name().casefold() in self.validDirNames:
                return mobase.ModDataChecker.VALID
        return mobase.ModDataChecker.INVALID


class BattletechGame(BasicGame):
    Name = "Battletech Support Plugin"
    Author = "Xyifer"
    Version = "2"
    Description = "Adds support for Battletech addons."

    GameName = "Battletech"
    GameShortName = "battletech"
    GameNexusName = "battletech"
    GameBinary = "BattleTech.exe"
    GameDataPath = "%GAME_PATH%/Mods"
    GameSteamId = 637090
	
    def init(self, organizer: mobase.IOrganizer) -> bool:
        super().init(organizer)
        self._register_feature(BattletechModDataChecker())
        return True

    def executables(self):
        return [
            mobase.ExecutableInfo(
                "Battletech",
                QFileInfo(self.gameDirectory().absoluteFilePath(self.binaryName())),
            ),
        ]
