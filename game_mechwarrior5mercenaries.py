# -*- encoding: utf-8 -*-

import os

import mobase
from PyQt6.QtCore import QDir, QFileInfo

from ..basic_game import BasicGame


class MechWarrior5MercenariesModDataChecker(mobase.ModDataChecker):
    def __init__(self):
        super().__init__()
        self.validDirNames = [
        "MW5Mercs/Mods",
    ]

    def dataLooksValid(
        self, filetree: mobase.IFileTree
    ) -> mobase.ModDataChecker.CheckReturn:
        for entry in filetree:
            if not entry.isDir():
                continue
            if entry.name().casefold() in self.validDirNames:
                return mobase.ModDataChecker.VALID
        return mobase.ModDataChecker.INVALID


class MechWarrior5MercenariesGame(BasicGame):
    Name = "MechWarrior 5 Mercenaries Support Plugin"
    Author = "Xyifer"
    Version = "2"
    Description = "Adds support for MechWarrior 5 Mercenaries addons."

    GameName = "MechWarrior 5 Mercenaries"
    GameShortName = "mechwarrior5"
    GameNexusName = "mechwarrior5mercenaries"
    GameBinary = "MechWarrior.exe"
    GameDataPath = "%GAME_PATH%/MW5Mercs/Mods"
    GameGogId = 2147483045
    GameSteamId = 784080
	
    def init(self, organizer: mobase.IOrganizer) -> bool:
        super().init(organizer)
        self._register_feature(MechWarrior5MercenariesModDataChecker())
        return True

    def executables(self):
        return [
            mobase.ExecutableInfo(
                "MechWarrior 5 Mercenaries",
                QFileInfo(self.gameDirectory().absoluteFilePath(self.binaryName())),
            ),
        ]
        
