# -*- encoding: utf-8 -*-

import os

import mobase
from PyQt6.QtCore import QDir, QFileInfo

from ..basic_game import BasicGame


class SPOREModDataChecker(mobase.ModDataChecker):
    def __init__(self):
        super().__init__()
        self.validDirNames = [
		"Data",
        "DataEP1",
    ]

    def dataLooksValid(
        self, filetree: mobase.IFileTree
    ) -> mobase.ModDataChecker.CheckReturn:
        for entry in filetree:
            if not entry.isDir():
                continue
            if entry.name().casefold() in self.validDirNames:
                return mobase.ModDataChecker.VALID
        return mobase.ModDataChecker.INVALID


class SPOREGame(BasicGame):
    Name = "SPORE Support Plugin"
    Author = "Xyifer"
    Version = "2"
    Description = "Adds support for SPORE addons."

    GameName = "SPORE"
    GameShortName = "spore"
    GameBinary = "SporeBin/SporeApp.exe"
    GameDataPath = "%GAME_PATH%"
    #GameSaveExtension = "spo"
    #GameSavesDirectory = "%AppData%/Roaming/Spore/Games/Game0/"
    GameSteamId = 17390
    GameGogId = 1948823323

    def init(self, organizer: mobase.IOrganizer) -> bool:
        super().init(organizer)
        self._register_feature(SPOREModDataChecker())
        return True

    def executables(self):
        return [
            mobase.ExecutableInfo(
                "SPORE",
                QFileInfo(self.gameDirectory().absoluteFilePath("../SporeApp.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
            mobase.ExecutableInfo(
                "SPORE Galactic Adventures",
                QFileInfo(self.gameDirectory().absoluteFilePath("../SporeBinEP1/SporeApp.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
            mobase.ExecutableInfo(
                "GOG Language Setup",
                QFileInfo(self.gameDirectory().absoluteFilePath("../language_setup.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
        ]
