# -*- encoding: utf-8 -*-

import os

import mobase
from PyQt6.QtCore import QDir, QFileInfo

from ..basic_game import BasicGame


class StarWarsJediKnightJediOutcastModDataChecker(mobase.ModDataChecker):
    def __init__(self):
        super().__init__()
        self.validDirNames = [
        "GameData/base",
    ]

    def dataLooksValid(
        self, filetree: mobase.IFileTree
    ) -> mobase.ModDataChecker.CheckReturn:
        for entry in filetree:
            if not entry.isDir():
                continue
            if entry.name().casefold() in self.validDirNames:
                return mobase.ModDataChecker.VALID
        return mobase.ModDataChecker.INVALID


class StarWarsJediKnightJediOutcastGame(BasicGame):
    Name = "Star Wars Jedi Knight - Jedi Outcast Support Plugin"
    Author = "Xyifer"
    Version = "2"
    Description = "Adds support for Star Wars Jedi Knight - Jedi Outcast addons."

    GameName = "STAR WARS Jedi Knight - Jedi Outcast"
    GameShortName = "starwarsjedioutcast"
    GameBinary = "GameData/jk2sp.exe"
    GameLauncher = "JediOutcast.exe"
    GameDataPath = "GameData/base"
    GameSaveExtension = "sav"
    GameSavesDirectory = "%GAME_PATH%/GameData/base/saves"
    GameSteamId = 6030
    GameGogId = 1428935917

    def init(self, organizer: mobase.IOrganizer) -> bool:
        super().init(organizer)
        self._register_feature(StarWarsJediKnightJediOutcastModDataChecker())
        return True

    def executables(self):
        return [              
            mobase.ExecutableInfo(
                "Singleplayer",
                QFileInfo(self.gameDirectory().absoluteFilePath("../jk2sp.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
            mobase.ExecutableInfo(
                "Multiplayer",
                QFileInfo(self.gameDirectory().absoluteFilePath("../jk2mp.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
        ]
