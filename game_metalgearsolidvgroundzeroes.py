# -*- encoding: utf-8 -*-

import os

import mobase
from PyQt6.QtCore import QDir, QFileInfo

from ..basic_game import BasicGame


class MetalGearRisingModDataChecker(mobase.ModDataChecker):
    def __init__(self):
        super().__init__()
        self.validDirNames = [
        "%GAME_PATH%",
    ]

    def dataLooksValid(
        self, filetree: mobase.IFileTree
    ) -> mobase.ModDataChecker.CheckReturn:
        for entry in filetree:
            if not entry.isDir():
                continue
            if entry.name().casefold() in self.validDirNames:
                return mobase.ModDataChecker.VALID
        return mobase.ModDataChecker.INVALID

class MGSVGroundZeroesGame(BasicGame):
    Name = "MGS V Ground Zeroes Support Plugin"
    Author = "Xyifer"
    Version = "2"
    Description = "Adds support for MGS V Ground Zeroes mods."

    GameName = "Metal Gear Solid V Ground Zeroes"
    GameShortName = "MGSV Ground Zeroes"
    GameBinary = "MgsGroundZeroes.exe"
    GameDataPath = ""
    GameSteamId = 311340
    
    def init(self, organizer: mobase.IOrganizer) -> bool:
        super().init(organizer)
        self._register_feature(MetalGearRisingModDataChecker())
        return True

    def executables(self):
        return [
            mobase.ExecutableInfo(
                "MGSV - Ground Zeroes",
                QFileInfo(self.gameDirectory().absoluteFilePath(self.binaryName())),
            ),
        ]

