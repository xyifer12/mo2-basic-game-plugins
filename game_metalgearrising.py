# -*- encoding: utf-8 -*-

import os

import mobase
from PyQt6.QtCore import QDir, QFileInfo

from ..basic_game import BasicGame


class MetalGearRisingModDataChecker(mobase.ModDataChecker):
    def __init__(self):
        super().__init__()
        self.validDirNames = [
        "%GAME_PATH%",
    ]

    def dataLooksValid(
        self, filetree: mobase.IFileTree
    ) -> mobase.ModDataChecker.CheckReturn:
        for entry in filetree:
            if not entry.isDir():
                continue
            if entry.name().casefold() in self.validDirNames:
                return mobase.ModDataChecker.VALID
        return mobase.ModDataChecker.INVALID


class MetalGearRisingGame(BasicGame):
    Name = "Metal Gear Rising Support Plugin"
    Author = "Xyifer"
    Version = "2"
    Description = "Adds support for Metal Gear Rising mods."

    GameName = "Metal gear Rising"
    GameShortName = "MGR"
    GameBinary = "MGR.exe"
    GameDataPath = "%GAME_PATH%"
    #GameSaveExtension = ""
    #GameSavesDirectory = ""
    GameSteamId = 235460
    GameGogId = 1777132927

    def init(self, organizer: mobase.IOrganizer) -> bool:
        super().init(organizer)
        self._register_feature(MetalGearRisingModDataChecker())
        return True

    def executables(self):
        return [
            mobase.ExecutableInfo(
                "Metal Gear Rising",
                QFileInfo(self.gameDirectory().absoluteFilePath(self.binaryName())),
            ),
        ]
