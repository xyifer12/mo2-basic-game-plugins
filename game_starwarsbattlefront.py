# -*- encoding: utf-8 -*-

import os

import mobase
from PyQt6.QtCore import QDir, QFileInfo

from ..basic_game import BasicGame


class StarWarsBattlefrontModDataChecker(mobase.ModDataChecker):
    def __init__(self):
        super().__init__()
        self.validDirNames = [
        "gamedata/addon",
		"gamedata/data",
    ]

    def dataLooksValid(
        self, filetree: mobase.IFileTree
    ) -> mobase.ModDataChecker.CheckReturn:
        for entry in filetree:
            if not entry.isDir():
                continue
            if entry.name().casefold() in self.validDirNames:
                return mobase.ModDataChecker.VALID
        return mobase.ModDataChecker.INVALID


class StarWarsBattlefrontGame(BasicGame):
    Name = "Star Wars Battlefront Support Plugin"
    Author = "Xyifer"
    Version = "2"
    Description = "Adds support for Star Wars Battlefront addons and mods."

    GameName = "Star Wars Battlefront"
    GameShortName = "starwarsbattlefront"
    GameBinary = "GameData/Battlefront.exe"
    GameDataPath = "GameData"
    GameSaveExtension = "profile2"
    GameSavesDirectory = "%GAME_PATH%/GameData/SaveGames"
    GameNexusName = "battlefront"
    GameSteamId = 1058020
    GameGogId = 1668107107

    def init(self, organizer: mobase.IOrganizer) -> bool:
        super().init(organizer)
        self._register_feature(StarWarsBattlefrontModDataChecker())
        return True

    def executables(self):
        return [              
            mobase.ExecutableInfo(
                "Star Wars Battlefront",
                QFileInfo(self.gameDirectory().absoluteFilePath(self.binaryName())),
            ),
            mobase.ExecutableInfo(
                "Launcher",
                QFileInfo(self.gameDirectory().absoluteFilePath("../LaunchBF.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
        ]
