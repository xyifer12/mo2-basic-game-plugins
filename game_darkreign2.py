# -*- encoding: utf-8 -*-

import os

import mobase
from PyQt6.QtCore import QDir, QFileInfo

from ..basic_game import BasicGame


class DarkReign2ModDataChecker(mobase.ModDataChecker):
    def __init__(self):
        super().__init__()
        self.validDirNames = [
            "mods",
            "mods/addon",
            "mods/personality",
            "mods/ruleset",
    ]

    def dataLooksValid(
        self, filetree: mobase.IFileTree
    ) -> mobase.ModDataChecker.CheckReturn:
        for entry in filetree:
            if not entry.isDir():
                continue
            if entry.name().casefold() in self.validDirNames:
                return mobase.ModDataChecker.VALID
        return mobase.ModDataChecker.INVALID


class DarkReign2Game(BasicGame):
    Name = "Dark Reign 2 Support Plugin"
    Author = "Xyifer"
    Version = "2"
    Description = "Adds support for Dark Reign 2 addons."

    GameName = "Dark Reign 2"
    GameShortName = "darkreign2"
    GameBinary = "dr2.exe"
    GameDataPath = "mods"
    GameSavesDirectory = "%GAME_PATH%/users"
    GameGogId = 1207658911

    def init(self, organizer: mobase.IOrganizer) -> bool:
        super().init(organizer)
        self._register_feature(DarkReign2ModDataChecker())
        return True

    def executables(self):
        return [              
            mobase.ExecutableInfo(
                "Dark Reign 2",
                QFileInfo(self.gameDirectory().absoluteFilePath(self.binaryName())),
            ),
            mobase.ExecutableInfo(
                "Dark Reign 2 Launcher",
                QFileInfo(self.gameDirectory().absoluteFilePath("../Launcher.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
        ]
