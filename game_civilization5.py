# -*- encoding: utf-8 -*-

import os

import mobase
from PyQt6.QtCore import QDir, QFileInfo

from ..basic_game import BasicGame


class CivilizationVModDataChecker(mobase.ModDataChecker):
    def __init__(self):
        super().__init__()
        self.validDirNames = [
        "Assets/DLC",
        "Assets",
    ]

    def dataLooksValid(
        self, filetree: mobase.IFileTree
    ) -> mobase.ModDataChecker.CheckReturn:
        for entry in filetree:
            if not entry.isDir():
                continue
            if entry.name().casefold() in self.validDirNames:
                return mobase.ModDataChecker.VALID
        return mobase.ModDataChecker.INVALID


class CivilizationVGame(BasicGame):
    Name = "Civilization V Support Plugin"
    Author = "Xyifer"
    Version = "2"
    Description = "Adds support for Civilization V fake DLC mods."

    GameName = "Sid Meier's Civilization V"
    GameShortName = "Civilization V"
    GameNexusName = "civilisationv"
    GameBinary = "CivilizationV.exe"
    GameDocumentsDirectory = "%DOCUMENTS%/My Games/Sid Meier's Civilization 5/"
    GameDataPath = "Assets/DLC"
    GameLauncher = "Launcher.exe"
    GameSaveExtension = "Civ5Save"
    GameSavesDirectory = "%DOCUMENTS%/My Games/Sid Meier's Civilization 5/Saves"
    GameSteamId = 8930

    def init(self, organizer: mobase.IOrganizer) -> bool:
        super().init(organizer)
        self._register_feature(CivilizationVModDataChecker())
        return True
        
    def iniFiles(self):
        return ["config.ini", "GraphicsSettingsDX9.ini", "GraphicsSettingsDX11.ini", "UserSettings.ini"]

    def executables(self):
        return [              
            mobase.ExecutableInfo(
                "Civilization V DX9",
                QFileInfo(self.gameDirectory().absoluteFilePath(self.binaryName())),
            ),
            mobase.ExecutableInfo(
                "Civilization V DX11",
                QFileInfo(self.gameDirectory().absoluteFilePath("../CivilizationV_DX11.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
            mobase.ExecutableInfo(
                "Civilization V Tablet",
                QFileInfo(self.gameDirectory().absoluteFilePath("../CivilizationV_Tablet.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
        ]
