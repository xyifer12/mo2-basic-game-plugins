# -*- encoding: utf-8 -*-

import os, shutil
from typing import List, Optional
from PyQt6.QtCore import QDir, QFileInfo, QDirIterator, QFile, QFileInfo, qDebug

import mobase

from ..basic_features import BasicLocalSavegames, BasicModDataChecker
from ..basic_game import BasicGame


class EarthDefenseForce5Game(BasicGame, mobase.IPluginFileMapper):
    Name = "Earth Defense Force 5 Support Plugin"
    Description = "Adds support for Earth Defense Force 5 addons."
    Author = "Xyifer"
    Version = "2"

    GameName = "Earth Defense Force 5"
    GameShortName = "EDF5"
    GameNexusName = "earthdefenseforce5"
    GameBinary = "EDF5.exe"
    GameDataPath = "%GAME_PATH%"
    #GameSaveExtension = ""
    GameSteamId = [1007040]
    #GameNexusId = []
    GameSavesDirectory = "%DOCUMENTS%/My Games/EDF5/SAVE_DATA/"
    
    CPK_MOD_PREFIX = "CPK_FILES"
    LOOSE_MOD_PREFIX = "LOOSE_FILES"
    MODLOADER_PLUGIN_PREFIX = "MODLOADER_PLUGINS"
    MODLOADER_PATCH_PREFIX = "MODLOADER_PATCHES"

    def __init__(self):
        BasicGame.__init__(self)
        mobase.IPluginFileMapper.__init__(self)

    def init(self, organizer: mobase.IOrganizer):
        super().init(organizer)

        self._register_feature(EarthDefenseForce5ModDataChecker())
        self._register_feature(BasicLocalSavegames(self.savesDirectory()))

        self._organizer.onAboutToRun(self.onAboutToRun)

        return True

    def executables(self):
        return [
            mobase.ExecutableInfo(
                "Earth Defense Force 5",
                QFileInfo(self.gameDirectory().absoluteFilePath(self.binaryName())),
            ),
        ]
        
    def mappings(self) -> List[mobase.Mapping]:
        map = []

        modDirs = [self.LOOSE_MOD_PREFIX]
        self._listDirsRecursive(modDirs, prefix=self.LOOSE_MOD_PREFIX)
        for dir_ in modDirs:
            for file_ in self._organizer.findFiles(path=dir_, filter=lambda x: True):
                m = mobase.Mapping()
                m.createTarget = True
                m.isDirectory = False
                m.source = file_
                m.destination = os.path.join(
                    QDir("Mods"),
                    file_.split(self.LOOSE_MOD_PREFIX)[1].strip("\\").strip("/"),
                )
                map.append(m)
        
        pluginDirs = [self.MODLOADER_PLUGIN_PREFIX]
        self._listDirsRecursive(configDirs, prefix=self.MODLOADER_PLUGIN_PREFIX)    
        for dir_ in configDirs:
            for file_ in self._organizer.findFiles(path=dir_, filter=lambda x: True):
                m = mobase.Mapping()
                m.createTarget = True
                m.isDirectory = os.path.isdir(file_)
                m.source = file_
                m.destination = os.path.join(
                    QDir("Plugins"),
                    file_.split(self.MODLOADER_PLUGIN_PREFIX)[1].strip("\\").strip("/"),
                )
                map.append(m)
                
        patchDirs = [self.MODLOADER_PATCH_PREFIX]
        self._listDirsRecursive(configDirs, prefix=self.MODLOADER_PATCH_PREFIX)    
        for dir_ in configDirs:
            for file_ in self._organizer.findFiles(path=dir_, filter=lambda x: True):
                m = mobase.Mapping()
                m.createTarget = True
                m.isDirectory = os.path.isdir(file_)
                m.source = file_
                m.destination = os.path.join(
                    QDir("Patches"),
                    file_.split(self.MODLOADER_PATCH_PREFIX)[1].strip("\\").strip("/"),
                )
                map.append(m)

        return map

    def _listDirsRecursive(self, dirs_list, prefix=""):
        dirs = self._organizer.listDirectories(prefix)
        for dir_ in dirs:
            dir_ = os.path.join(prefix, dir_)
            dirs_list.append(dir_)
            self._listDirsRecursive(dirs_list, dir_)

    def onModChanged(self, mod) -> bool:
        modsDict = [next(iter(mod))]
        ModSettingsHelper.generateSettings(self._organizer.modList(), self._organizer.profile(), modsDict)
        return True

    def onAboutToRun(self, mod):
        ModSettingsHelper.generateSettings(self._organizer.modList(), self._organizer.profile(), [])
        return True


class EarthDefenseForce5ModDataChecker(mobase.ModDataChecker):
    def __init__(self):
        super().__init__()
        
    def dataLooksValid(self, tree: mobase.IFileTree) -> mobase.ModDataChecker.CheckReturn:
        folders: List[mobase.IFileTree] = []
        files: List[mobase.FileTreeEntry] = []
        for entry in tree:
            if isinstance(entry, mobase.IFileTree):
                folders.append(entry)
            else:
                files.append(entry)

        VALID_FOLDERS = [
            "Mods",
            "DEFAULTPACKAGE",
            "ETC",
            "MENUOBJECT",
            "MISSION",
            "OBJECT",
            "Patches",
            "Plugins",
            "UI",
            "WEAPON",
            "SOUND",
            "SOUND/PC",
        ]
        
        VALID_FILE_EXTENSIONS = [
            ".asm",
            ".acb",
            ".awb",
            ".bvm",
            ".dll",
            ".json",
            ".mp4",
            ".mrab",
            ".rmpa",
            ".sgo",
            ".txt",
            ".txt_sgo", 
        ]
        
        for mainFolder in folders:
            for validFolder in VALID_FOLDERS:
                if mainFolder.name().lower() == validFolder.lower():
                    return mobase.ModDataChecker.VALID
                    
        for mainFile in files:
            for extension in VALID_FILE_EXTENSIONS:
                if mainFile.name().lower().endswith(extension.lower()) and mainFile.name() != "readme.txt":
                    return mobase.ModDataChecker.FIXABLE
                    
        for src_folder in folders:
            for dst_folder in VALID_FOLDERS:
                if src_folder.name().lower() == dst_folder.lower():
                    return mobase.ModDataChecker.VALID

        return mobase.ModDataChecker.INVALID
        
    def fix(self, tree: mobase.IFileTree) -> Optional[mobase.IFileTree]:
        folders: List[mobase.IFileTree] = []
        files: List[mobase.FileTreeEntry] = []
        for entry in tree:
            if isinstance(entry, mobase.IFileTree):
                folders.append(entry)
            else:
                files.append(entry)

        REMOVE_FILES = [
            "readme"
        ]
        REMOVE_FILE_EXTENSIONS = [
            ".url",
            ".html",
            ".ink"
        ]
        
        for mainFile in files:
            for extension in REMOVE_FILE_EXTENSIONS:
                if mainFile.name().lower().endswith(extension.lower()):
                     tree.remove(mainFile)
            for filename in REMOVE_FILES:
                if mainFile.name().lower() == filename:
                     tree.remove(mainFile)

        for mainFolder in folders:
            for mainFile in mainFolder:
                for extension in REMOVE_FILE_EXTENSIONS:
                    if mainFile.name().lower().endswith(extension.lower()):
                        tree.remove(mainFolder)
                for filename in REMOVE_FILES:
                    if mainFile.name().lower() == filename:
                       tree.remove(mainFolder)
                       
        for mainFile in files:
            if mainFile.name().lower().endswith(".txt".lower()) and mainFile.name() != "readme.txt":
                tree.move(mainFile, "/Mods/Patches/", policy=mobase.IFileTree.MERGE)
            if mainFile.name().lower().endswith(".dll".lower()):
                tree.move(mainFile, "/Mods/Plugins/", policy=mobase.IFileTree.MERGE)
                    
        for mainFolder in folders:
            if mainFolder.name().lower() == "DEFAULTPACKAGE":
                tree.move(mainFolder, "%GAME_PATH%/Mods/", policy=mobase.IFileTree.MERGE)
            if mainFolder.name().lower() == "ETC":
                tree.move(mainFolder, "%GAME_PATH%/Mods/", policy=mobase.IFileTree.MERGE)
            if mainFolder.name().lower() == "MENUOBJECT":
                tree.move(mainFolder, "%GAME_PATH%/Mods/", policy=mobase.IFileTree.MERGE)
            if mainFolder.name().lower() == "MISSION":
                tree.move(mainFolder, "%GAME_PATH%/Mods/", policy=mobase.IFileTree.MERGE)
            if mainFolder.name().lower() == "MOD":
                tree.move(mainFolder, "%GAME_PATH%/Mods/", policy=mobase.IFileTree.MERGE)
            if mainFolder.name().lower() == "OBJECT":
                tree.move(mainFolder, "%GAME_PATH%/Mods/", policy=mobase.IFileTree.MERGE)
            if mainFolder.name().lower() == "UI":
                tree.move(mainFolder, "%GAME_PATH%/Mods/", policy=mobase.IFileTree.MERGE)
            if mainFolder.name().lower() == "WEAPON":
                tree.move(mainFolder, "%GAME_PATH%/Mods/", policy=mobase.IFileTree.MERGE)
            else:
                for mainFile in mainFolder:
                    if mainFile is None: continue
                    if mainFile.name().lower().endswith(".txt".lower()) and mainFile.name() != "readme.txt":
                        tree.move(mainFile, "/Mods/Patches/", policy=mobase.IFileTree.MERGE)
                    if mainFile.name().lower().endswith(".dll".lower()):
                        tree.move(mainFile, "/Mods/Plugins/", policy=mobase.IFileTree.MERGE)
                       
        return tree

