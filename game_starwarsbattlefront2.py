# -*- encoding: utf-8 -*-

import os

import mobase
from PyQt6.QtCore import QDir, QFileInfo

from ..basic_game import BasicGame


class StarWarsBattlefront2ModDataChecker(mobase.ModDataChecker):
    def __init__(self):
        super().__init__()
        self.validDirNames = [
        "gamedata/addon",
		"gamedata/data",
    ]

    def dataLooksValid(
        self, filetree: mobase.IFileTree
    ) -> mobase.ModDataChecker.CheckReturn:
        for entry in filetree:
            if not entry.isDir():
                continue
            if entry.name().casefold() in self.validDirNames:
                return mobase.ModDataChecker.VALID
        return mobase.ModDataChecker.INVALID


class StarWarsBattlefront2Game(BasicGame):
    Name = "Star Wars Battlefront 2 Support Plugin"
    Author = "Xyifer"
    Version = "2"
    Description = "Adds support for Star Wars Battlefront 2 addons and mods."

    GameName = "Star Wars Battlefront 2"
    GameShortName = "starwarsbattlefront2"
    GameBinary = "GameData/BattlefrontII.exe"
    GameDataPath = "GameData"
    #GameLauncher = ""
    GameSaveExtension = "profile"
    GameSavesDirectory = "%GAME_PATH%/GameData/SaveGames"
    GameNexusName = "battlefront2"
    GameSteamId = 6060
    GameGogId = 1421404701

    def init(self, organizer: mobase.IOrganizer) -> bool:
        super().init(organizer)
        self._register_feature(StarWarsBattlefront2ModDataChecker())
        return True

    def executables(self):
        return [              
            mobase.ExecutableInfo(
                "Star Wars Battlefront 2",
                QFileInfo(self.gameDirectory().absoluteFilePath(self.binaryName())),
            ),
            mobase.ExecutableInfo(
                "Debug Executable",
                QFileInfo(self.gameDirectory().absoluteFilePath("../BF2_modtools.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
        ]
