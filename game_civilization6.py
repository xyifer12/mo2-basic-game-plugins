# -*- encoding: utf-8 -*-

import os

import mobase
from PyQt6.QtCore import QDir, QFileInfo

from ..basic_game import BasicGame


class CivilizationVIModDataChecker(mobase.ModDataChecker):
    def __init__(self):
        super().__init__()
        self.validDirNames = [
        "%GAME_DOCUMENTS%/Mods",
    ]

    def dataLooksValid(
        self, filetree: mobase.IFileTree
    ) -> mobase.ModDataChecker.CheckReturn:
        for entry in filetree:
            if not entry.isDir():
                continue
            if entry.name().casefold() in self.validDirNames:
                return mobase.ModDataChecker.VALID
        return mobase.ModDataChecker.INVALID


class CivilizationVIGame(BasicGame):
    Name = "Civilization VI Support Plugin"
    Author = "Xyifer"
    Version = "2"
    Description = "Adds support for Civilization VI mods."

    GameName = "Sid Meier's Civilization VI"
    GameShortName = "Civilization VI"
    GameNexusName = "civilisationvi"
    GameBinary = "CivilizationVI.exe"
    GameDocumentsDirectory = "%DOCUMENTS%/My Games/Sid Meier's Civilization VI/"
    GameDataPath = "Base/Binaries/Win64Steam"
    GameSaveExtension = "Civ6Save"
    GameSavesDirectory = "%GAME_DOCUMENTS%/Saves"
    GameSteamId = 289070

    def init(self, organizer: mobase.IOrganizer) -> bool:
        super().init(organizer)
        self._register_feature(CivilizationVIModDataChecker())
        return True

    def executables(self):
        return [              
            mobase.ExecutableInfo(
                "Civilization VI DX11",
                QFileInfo(self.gameDirectory().absoluteFilePath(self.binaryName())),
            ),
            mobase.ExecutableInfo(
                "Civilization VI DX12",
                QFileInfo(self.gameDirectory().absoluteFilePath("../CivilizationVI_DX12.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
        ]
