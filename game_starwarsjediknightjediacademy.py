# -*- encoding: utf-8 -*-

import os

import mobase
from PyQt6.QtCore import QDir, QFileInfo

from ..basic_game import BasicGame


class StarWarsJediKnightJediAcademyModDataChecker(mobase.ModDataChecker):
    def __init__(self):
        super().__init__()
        self.validDirNames = [
        "GameData/base",
    ]

    def dataLooksValid(
        self, filetree: mobase.IFileTree
    ) -> mobase.ModDataChecker.CheckReturn:
        for entry in filetree:
            if not entry.isDir():
                continue
            if entry.name().casefold() in self.validDirNames:
                return mobase.ModDataChecker.VALID
        return mobase.ModDataChecker.INVALID


class StarWarsJediKnightJediAcademyGame(BasicGame):
    Name = "Star Wars Jedi Knight - Jedi Academy Support Plugin"
    Author = "Xyifer"
    Version = "2"
    Description = "Adds support for Star Wars Jedi Knight - Jedi Academy addons."

    GameName = "STAR WARS Jedi Knight - Jedi Academy"
    GameShortName = "starwarsjediacademy"
    GameBinary = "GameData/jasp.exe"
    GameLauncher = "JediAcademy.exe"
    GameDataPath = "GameData/base"
    GameSaveExtension = "sav"
    GameSavesDirectory = "%GAME_PATH%/GameData/base/saves"
    GameSteamId = 6020
    GameGogId = 1428935726

    def init(self, organizer: mobase.IOrganizer) -> bool:
        super().init(organizer)
        self._register_feature(StarWarsJediKnightJediAcademyModDataChecker())
        return True

    def executables(self):
        return [              
            mobase.ExecutableInfo(
                "Singleplayer",
                QFileInfo(self.gameDirectory().absoluteFilePath("../jasp.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
            mobase.ExecutableInfo(
                "Multiplayer",
                QFileInfo(self.gameDirectory().absoluteFilePath("../jamp.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
            mobase.ExecutableInfo(
                "OpenJK Singleplayer",
                QFileInfo(self.gameDirectory().absoluteFilePath("../openjk_sp.x86.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
            mobase.ExecutableInfo(
                "OpenJK Multiplayer",
                QFileInfo(self.gameDirectory().absoluteFilePath("../openjk.x86.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
        ]
