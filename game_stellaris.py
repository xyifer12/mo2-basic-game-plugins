# -*- encoding: utf-8 -*-

import os

import mobase
from PyQt6.QtCore import QDir, QFileInfo

from ..basic_game import BasicGame


class StellarisModDataChecker(mobase.ModDataChecker):
    def __init__(self):
        super().__init__()
        self.validDirNames = [
            "%DOCUMENTS%/Paradox Interactive/Stellaris/mod",
            "common",
            "",
    ]

    def dataLooksValid(
        self, filetree: mobase.IFileTree
    ) -> mobase.ModDataChecker.CheckReturn:
        for entry in filetree:
            if not entry.isDir():
                continue
            if entry.name().casefold() in self.validDirNames:
                return mobase.ModDataChecker.VALID
        return mobase.ModDataChecker.INVALID


class StellarisGame(BasicGame):
    Name = "Stellaris Support Plugin"
    Author = "Xyifer"
    Version = "2"
    Description = "Adds support for Stellaris core-mods."

    GameName = "Stellaris"
    GameShortName = "stellaris"
    GameNexusName = "stellaris"
    GameBinary = "stellaris.exe"
    GameDocumentsDirectory = "%DOCUMENTS%/Paradox Interactive/Stellaris/"
    GameDataPath = ""
    GameLauncher = "dowser.exe"
    GameSaveExtension = "sav"
    GameSavesDirectory = "%DOCUMENTS%/Paradox Interactive/Stellaris/save games"
    GameSteamId = 281990
    GameGogId = 1508702879
	
    def iniFiles(self):
        return ["settings.txt", "pdx_settings.txt", "user_empire_designs.txt"]

    def init(self, organizer: mobase.IOrganizer) -> bool:
        super().init(organizer)
        self._register_feature(StellarisModDataChecker())
        return True

    def executables(self):
        return [              
            mobase.ExecutableInfo(
                "Stellaris",
                QFileInfo(self.gameDirectory().absoluteFilePath(self.binaryName())),
            ),
            mobase.ExecutableInfo(
                "Launcher",
                QFileInfo(self.gameDirectory().absoluteFilePath("../dowser.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
        ]
