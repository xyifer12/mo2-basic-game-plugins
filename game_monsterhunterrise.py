# -*- encoding: utf-8 -*-

import os

import mobase
from PyQt6.QtCore import QDir, QFileInfo

from ..basic_game import BasicGame


class MonsterHunterRiseModDataChecker(mobase.ModDataChecker):
    def __init__(self):
        super().__init__()
        self.validDirNames = [
        "",
		"reframework",
		"reframework/plugins",
		"reframework/autorun",
		"reframework/images",
    ]

    def dataLooksValid(
        self, filetree: mobase.IFileTree
    ) -> mobase.ModDataChecker.CheckReturn:
        for entry in filetree:
            if not entry.isDir():
                continue
            if entry.name().casefold() in self.validDirNames:
                return mobase.ModDataChecker.VALID
        return mobase.ModDataChecker.INVALID


class MonsterHunterRiseGame(BasicGame):
    Name = "Monster Hunter Rise Support Plugin"
    Author = "Xyifer"
    Version = "2"
    Description = "Adds support for Monster Hunter Rise mods."

    GameName = "Monster Hunter Rise"
    GameShortName = "monsterhunterrise"
    GameBinary = "MonsterHunterRise.exe"
    GameDataPath = ""
    GameSteamId = 1446780

    def init(self, organizer: mobase.IOrganizer) -> bool:
        super().init(organizer)
        self._register_feature(MonsterHunterRiseModDataChecker())
        return True

    def executables(self):
        return [          
            mobase.ExecutableInfo(
                "Monster Hunter Rise",
                QFileInfo(self.gameDirectory().absoluteFilePath(self.binaryName())),
            ),
        ]
