# -*- encoding: utf-8 -*-

import os

import mobase
from PyQt6.QtCore import QDir, QFileInfo

from ..basic_game import BasicGame


class StarboundModDataChecker(mobase.ModDataChecker):
    def __init__(self):
        super().__init__()
        self.validDirNames = [
        "mods",
    ]

    def dataLooksValid(
        self, filetree: mobase.IFileTree
    ) -> mobase.ModDataChecker.CheckReturn:
        for entry in filetree:
            if not entry.isDir():
                continue
            if entry.name().casefold() in self.validDirNames:
                return mobase.ModDataChecker.VALID
        return mobase.ModDataChecker.INVALID


class StarboundGame(BasicGame):
    Name = "Starbound Support Plugin"
    Author = "Xyifer"
    Version = "2"
    Description = "Adds support for Starbound addons."

    GameName = "Starbound"
    GameShortName = "starbound"
    GameBinary = "win64/starbound.exe"
    GameDataPath = "mods"
    GameSavesDirectory = "%GAME_PATH%/storage"
    GameSteamId = 211820
    GameGogId = 1452598881

    def init(self, organizer: mobase.IOrganizer) -> bool:
        super().init(organizer)
        self._register_feature(StarboundModDataChecker())
        return True

    def executables(self):
        return [
            mobase.ExecutableInfo(
                "Starbound",
                QFileInfo(self.gameDirectory().absoluteFilePath("../win64/starbound.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
            mobase.ExecutableInfo(
                "Starbound x86",
                QFileInfo(self.gameDirectory().absoluteFilePath("../win32/starbound.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
            mobase.ExecutableInfo(
                "Starbound Server",
                QFileInfo(self.gameDirectory().absoluteFilePath("../win64/starbound_server.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
            mobase.ExecutableInfo(
                "Starbound x86 Server",
                QFileInfo(self.gameDirectory().absoluteFilePath("../win32/starbound_server.exe")),
            ).withWorkingDirectory(
                QDir(QDir.cleanPath(self.gameDirectory().absoluteFilePath("..")))
            ),
        ]
