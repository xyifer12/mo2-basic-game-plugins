# -*- encoding: utf-8 -*-

import os

import mobase
from PyQt6.QtCore import QDir, QFileInfo

from ..basic_game import BasicGame


class DRAGONBALLSparkingZEROModDataChecker(mobase.ModDataChecker):
    def __init__(self):
        super().__init__()
        self.validDirNames = [
        "SparkingZERO/Content/Paks",
        "SparkingZERO/Content/Paks/~mods",
    ]

    def dataLooksValid(
        self, filetree: mobase.IFileTree
    ) -> mobase.ModDataChecker.CheckReturn:
        for entry in filetree:
            if not entry.isDir():
                continue
            if entry.name().casefold() in self.validDirNames:
                return mobase.ModDataChecker.VALID
        return mobase.ModDataChecker.INVALID


class DRAGONBALLSparkingZEROGame(BasicGame):
    Name = "DRAGON BALL: Sparking! ZERO Support Plugin"
    Author = "Xyifer"
    Version = "1"
    Description = "Adds support for DRAGON BALL: Sparking! ZERO DLLs and packed mods."

    GameName = "DRAGON BALL: Sparking! ZERO"
    GameShortName = "SparkingZERO"
    GameNexusName = "dragonballsparkingzero"
    GameBinary = "SparkingZERO.exe"
    GameDataPath = "%GAME_PATH%"
    GameDocumentsDirectory = "%LOCALAPPDATA%/SparkingZERO/Saved/Config/Windows/"
    GameSaveExtension = ""
    GameSavesDirectory = "%GAME_PATH%/SaveGame/"
    GameSteamId = 1790600

    def init(self, organizer: mobase.IOrganizer) -> bool:
        super().init(organizer)
        self._register_feature(DRAGONBALLSparkingZEROModDataChecker())
        return True
        
    def iniFiles(self):
        return ["engine.ini", "GameUserSettings.ini"]
        
    def executables(self):
        return [              
            mobase.ExecutableInfo(
                "DRAGON BALL: Sparking! ZERO",
                QFileInfo(self.gameDirectory().absoluteFilePath(self.binaryName())),
            ),
        ]

